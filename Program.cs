﻿/*-----------------------------------
Лабораторная работа №1
ФИО: Винокуров Александр Анатольевич
-----------------------------------*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace NotebookApp
{
    class Notebook // Основной класс
    {
        public static string name; // Переменная, чтобы хранить имя пользователя
        public static List<Person> ListNotebook = new List<Person>(); // Лист для хранения записей в телефоной книжке
        static void Main(string[] args)
        {
            string comand; // Переменная, чтобы хранить команду
            Console.WriteLine("Я консольное приложение: Записная телефонная книжка"); // Приветсвие
            Console.WriteLine("Давайте познакомимся!");
            Console.WriteLine("Введите Ваше имя: "); // Просим пользователя ввести свое имя
            name = Console.ReadLine();
            while (String.IsNullOrEmpty(name)) // Если пользователь не введет свое имя
            {
                Console.WriteLine("С незнакомцами не общаюсь!!!");
                Console.WriteLine("Введите Ваше имя: ");
                name = Console.ReadLine();
            }
            Console.Clear();
            Console.WriteLine($"Привет, {name}!"); // Приветствуем пользователя
            while (true)
            {
                Console.WriteLine("Введите слово 'Add', чтобы добавить новую запись в записную книжку"); // Вывод команд в консоль и их объяснение
                Console.WriteLine("Введите слово 'Edit', чтобы редактировать запись в записной книжке");
                Console.WriteLine("Введите слово 'Del', чтобы удалить запись в записной книжке");
                Console.WriteLine("Введите слово 'ViewAll', чтобы просмотреть созданные учетные записи (с полной информацией)");
                Console.WriteLine("Введите слово 'View', чтобы просмотреть все записи в записной книжке, с краткой информацией (Фамилия, Имя, Номер телефона)");
                Console.WriteLine("Введите слово 'Exit', чтобы выйти из приложения");
                Console.WriteLine($"{name}, введите команду: "); // Просим пользователя ввести команду
                comand = Console.ReadLine();
                if (comand == "Add" || comand == "Edit" || comand == "Del" || comand == "ViewAll" || comand == "View" || comand == "Exit") // Если команда написана правильно
                {
                    if (comand == "Add") // Добавить новую запись
                    {
                        Console.Clear(); // Отчистить консоль от предыдущих записей 
                        CreateNewNote(); // Вызов метода отвечающего за создание новой записи в телефоную книжку
                        Console.Clear(); // Отчистить консоль от предыдущих записей
                        Console.WriteLine("В записную телефонную книжку будет добавлена следующая запись: "); // Выводим в консоль новую созданную запись
                        PrintNote(ListNotebook.Count - 1); // Вывод в консоль новую запись
                        Console.WriteLine($"{ name}, нажмите любую клавишу, чтобы продолжить!"); // Ждем пока пользователь захочет продолжить (Ожидаем нажатие любой клавиши)
                        Console.ReadKey();
                        Console.Clear(); // Отчистить консоль от предыдущих записей 
                        continue; // Возращаемся к началу цикла
                    }
                    if (comand == "Edit") // Изменить запись
                    {
                        Console.Clear();
                        if (ListNotebook.Count > 0) // Если в ListNotebook есть записи
                        {
                            Console.WriteLine($"{name}, давайте найдем запись, чтобы ее отредактировать!");
                            int indexEdit = FindNote(); // Вызываем метод, который найдет запись, которую пользователь хочет отредактировать
                            if (indexEdit != -1) // Если запись нашлась
                            {
                                Console.Clear();
                                Console.WriteLine("Успешно, запись найдена!"); // Выводим найденую запись
                                PrintNote(indexEdit); // Вывод в консоль запись, которую пользователь начнет редактирвать
                                Console.WriteLine("Давайте начнем редактировать запись!");
                                EditNote(indexEdit); // Метод, который редактирует запись
                            }
                        } else // Если в телефоной книжке нет еще записей
                        {
                            Console.WriteLine($"{name}, в телефонной книжке нет еще записей!");
                            Console.WriteLine("Добавьте хотя бы одну запись в телефонную книгу, чтобы появилась возможность редактировать записи");
                        }
                        Console.WriteLine($"{ name}, нажмите любую клавишу, чтобы продолжить!"); // Ждем пока пользователь захочет продолжить (Ожидаем нажатие любой клавиши)
                        Console.ReadKey();
                        Console.Clear(); // Отчистить консоль от предыдущих записей 
                        continue; // Возращаемся к началу цикла
                    }
                    if (comand == "Del") // Удалить запись
                    {
                        Console.Clear(); // Очистить консоль от предыдущих записей 
                        if (ListNotebook.Count > 0) // Если в ListNotebook есть записи
                        {
                            Console.WriteLine("Вы хотите удалить все записи или только конкретную?"); // Спрашиваем пользователя
                            Console.WriteLine("Если все записи, то введите: All");
                            Console.WriteLine("Если конкретную запись, то введите: OnlyOne");
                            Console.WriteLine("Введите команду: ");
                            string DelComand = Console.ReadLine();
                            while (String.IsNullOrEmpty(DelComand) || DelComand != "All" || DelComand != "OnlyOne") // Если пользователь ввел некорректную команду 
                            {
                                if (DelComand == "All" || DelComand == "OnlyOne")
                                {
                                    break;
                                }
                                Console.WriteLine("Непонятная команда!");
                                Console.WriteLine("Введите команду: ");
                                DelComand = Console.ReadLine();
                            }
                            Console.Clear();
                            if (DelComand == "All") // Если выбрал все записи
                            {
                                ListNotebook.Clear(); // Удаляем все записи из записной книжки
                                Console.WriteLine($"{name}, все записи удалены!");
                            }
                            if (DelComand == "OnlyOne") // Если выбрал только одну
                            {
                                Console.WriteLine($"{name}, давайте найдем запись, чтобы ее удалить!");
                                int indexDel = FindNote(); // Находим индекс строки в ListNotebook
                                if (indexDel != -1) // Если нашли запись
                                {
                                    Console.Clear();
                                    Console.WriteLine("Успешно, запись найдена! Она будет удалена!"); // Выводим найденую запись
                                    PrintNote(indexDel); // Вывод в консоль запись, которую пользователь удалит
                                    ListNotebook.Remove(ListNotebook[indexDel]); // Удаляем запись
                                }
                            }
                        }
                        else // Если в телефоной книжке нет еще записей
                        {
                            Console.WriteLine($"{name}, в телефонной книжке нет еще записей!");
                            Console.WriteLine("Добавьте хотя бы одну запись в телефонную книгу, чтобы появилась возможность удалять записи");
                        }
                        Console.WriteLine($"{ name}, нажмите любую клавишу, чтобы продолжить!"); // Ждем пока пользователь захочет продолжить (Ожидаем нажатие любой клавиши)
                        Console.ReadKey();
                        Console.Clear(); // Отчистить консоль от предыдущих записей 
                        continue; // Возращаемся к началу цикла
                    }
                    if (comand == "ViewAll") // Посмотреть всю информацию
                    {
                        Console.Clear();
                        if (ListNotebook.Count > 0) // Если в ListNotebook есть записи
                        {
                            Console.WriteLine("Вы хотите посмотреть все записи или только конкретную?");
                            Console.WriteLine("Если все записи, то введите: All");
                            Console.WriteLine("Если конкретную запись, то введите: OnlyOne");
                            Console.WriteLine("Введите команду: ");
                            string ViewAllComand = Console.ReadLine();
                            while (String.IsNullOrEmpty(ViewAllComand) || ViewAllComand != "All" || ViewAllComand != "OnlyOne") // Если пользователь ввел некорректную команду 
                            {
                                if (ViewAllComand == "All" || ViewAllComand == "OnlyOne")
                                {
                                    break;
                                }
                                Console.WriteLine("Непонятная команда!");
                                Console.WriteLine("Введите команду: ");
                                ViewAllComand = Console.ReadLine();
                            }
                            Console.Clear();
                            if (ViewAllComand == "All") // Если пользователь выбрал все записи
                            {
                                List<int> ViewAll = new List<int>(); // Создаем лист с индексами 
                                for (int i = 0; i < ListNotebook.Count; i++)
                                {
                                    ViewAll.Add(i);
                                }
                                PrintNotesWithNum(ViewAll); // Выводи в консоль все записи
                            }
                            if (ViewAllComand == "OnlyOne") // Если только одну запись
                            {
                                Console.WriteLine($"{name}, давайте найдем запись, чтобы ее увидеть!");
                                int indexDel = FindNote(); // Находим индекс записи
                                if (indexDel != -1)
                                {
                                    Console.Clear();
                                    Console.WriteLine("Успешно, запись найдена!");
                                    PrintNote(indexDel); // Вывод в консоль запись
                                }
                            }
                        }
                        else // Если нет еще записей
                        {
                            Console.WriteLine($"{name}, в телефонной книжке нет еще записей!");
                            Console.WriteLine("Добавьте хотя бы одну запись в телефонную книгу, чтобы появилась возможность смотреть записи");
                        }
                        Console.WriteLine($"{ name}, нажмите любую клавишу, чтобы продолжить!"); // Ждем пока пользователь захочет продолжить (Ожидаем нажатие любой клавиши)
                        Console.ReadKey();
                        Console.Clear(); // Отчистить консоль от предыдущих записей 
                        continue; // Возращаемся к началу цикла
                    }
                    if (comand == "View") // Увидель краткую информацию 
                    {
                        Console.Clear();
                        if (ListNotebook.Count > 0) // Если есть записи в телефоной книжке 
                        {
                            Console.WriteLine("{0,3} {1,20} {2,20} {3,14}", "№", "Фамилия", "Имя", "Номер телефона"); // Вывод всех записей в краткой форме 
                            int numberView = 1;
                            foreach (var i in ListNotebook)
                            {
                                Console.WriteLine("{0,3} {1,20} {2,20} {3,14}", numberView, i.surname, i.name, i.phoneNumber);
                                numberView++;
                            }
                        } else // Если нет еще записей в телефоной книжке
                        {
                            Console.WriteLine($"{name}, в телефонной книжке нет еще записей!");
                            Console.WriteLine("Добавьте хотя бы одну запись в телефонную книгу, чтобы появилась возможность смотреть записи");
                        }
                        Console.WriteLine($"{ name}, нажмите любую клавишу, чтобы продолжить!"); // Ждем пока пользователь захочет продолжить (Ожидаем нажатие любой клавиши)
                        Console.ReadKey();
                        Console.Clear(); // Отчистить консоль от предыдущих записей 
                        continue; // Возращаемся к началу цикла
                    }
                    if (comand == "Exit") // Выход из программы
                    {
                        break;
                    }
                } else // Если команда написана не правильно, то просим пользователя повторить ввод команды
                {
                    Console.Clear();
                    Console.WriteLine("Непонятная команда!");
                }
            }
        }
        
        static void PrintNote (int index) // Метод, который выводит в консоль запись по определенному индексу 
        {
            Console.WriteLine("{0,20} {1,20} {2,20} {3,14} {4, 15} {5, 15} {6, 15} {7, 15} {8, 15}", "Фамилия", "Имя", "Отчество", "Номер телефона", "Страна", "Дата рождения",
                            "Организация", "Должность", "Прочие заметки"); // Вывод шапки
            Console.Write("{0,20} {1,20} {2,20} {3,14} {4, 15} ", ListNotebook[index].surname, ListNotebook[index].name, ListNotebook[index].middleName, ListNotebook[index].phoneNumber,
                ListNotebook[index].country); // Вывод фамилии, имени, отчества, номера телефона, страны
            if (ListNotebook[index].dataBirthday != DateTime.MinValue) // Если дата введена, то выводим в формате: dd.MM.yyyy
            {
                Console.Write("{0, 15} ", ListNotebook[index].dataBirthday.ToString("dd.MM.yyyy"));
            }
            else // Если дата не указана 
            {
                Console.Write("{0, 15} ", ListNotebook[index].dataBirthday.ToString("не указано"));
            }
            Console.WriteLine("{0, 15} {1, 15} {2, 15}", ListNotebook[index].organization, ListNotebook[index].position, ListNotebook[index].other); // Вывод организации, должности, прочих заметок
        }

        static void PrintNotesWithNum (List<int> index) // Вывод нескольких записей 
        {
            Console.WriteLine("{0,3} {1,20} {2,20} {3,20} {4,14} {5, 15} {6, 15} {7, 15} {8, 15} {9, 15}", "№", "Фамилия", "Имя", "Отчество", "Номер телефона", "Страна", "Дата рождения",
                        "Организация", "Должность", "Прочие заметки"); // Вывод шапки 
            int number = 1; // Индекс строки
            foreach (int i in index)
            {
                Console.Write("{0,3} {1,20} {2,20} {3,20} {4,14} {5, 15} ", number, ListNotebook[i].surname, ListNotebook[i].name, ListNotebook[i].middleName, ListNotebook[i].phoneNumber,
                    ListNotebook[i].country); // Вывод индекса, фамилии, имени, отчества, номера телефона, страны
                if (ListNotebook[i].dataBirthday != DateTime.MinValue) // Если дата введена, то выводим в формате: dd.MM.yyyy
                {
                    Console.Write("{0, 15} ", ListNotebook[i].dataBirthday.ToString("dd.MM.yyyy"));
                }
                else // Если дата не указана 
                {
                    Console.Write("{0, 15} ", ListNotebook[i].dataBirthday.ToString("не указано"));
                }
                Console.WriteLine("{0, 15} {1, 15} {2, 15}", ListNotebook[i].organization, ListNotebook[i].position, ListNotebook[i].other); // Вывод организации, должности, прочих заметок
                number++; // Увеличиваем индекс на 1
            }
        }

        static void CreateNewNote() // Метод отвечающий за создание новой записи
        {
            Console.WriteLine("Введите фамилию (это поле является обязательным!): "); // Просим ввести фамилию для записи в телефоной книжки
            string surname = Console.ReadLine();
            while (String.IsNullOrEmpty(surname)) // Если пользователь не введет фамилию (обязательное поле)
            {
                Console.WriteLine($"{name}, Вы ввели пустую строку! ");
                Console.WriteLine("Введите фамилию (это поле является обязательным!): ");
                surname = Console.ReadLine();
            }
            Console.Clear();
            Console.WriteLine("Введите имя (это поле является обязательным!): "); // Просим ввести имя для записи в телефоной книжки
            string nameNote = Console.ReadLine();
            while (String.IsNullOrEmpty(nameNote)) // Если пользователь не введет имя (обязательное поле)
            {
                Console.WriteLine($"{name}, Вы ввели пустую строку! ");
                Console.WriteLine("Введите имя (это поле является обязательным!): ");
                nameNote = Console.ReadLine();
            }
            Console.Clear();
            Console.WriteLine("Введите отчество (это поле можно оставить пустым): "); // Просим ввести отчество для записи в телефоной книжки
            string middleName = Console.ReadLine();
            Console.Clear();
            Console.WriteLine("Введите номер телефона (это поле является обязательным! Номер телефона должен состоять из цифр!): "); // Просим ввести номер телефона для записи в телефоной книжки
            string phoneNumber = Console.ReadLine();
            long x;
            while (String.IsNullOrEmpty(phoneNumber) || !Int64.TryParse(phoneNumber, out x)) // Если пользователь не введет номер телефона или номер введен некорректно (обязательное поле)
            {
                if (String.IsNullOrEmpty(phoneNumber)) // Если пользователь ввел пустую строку
                {
                    Console.WriteLine($"{name}, Вы ввели пустую строку! ");
                    Console.WriteLine("Введите номер телефона (это поле является обязательным!): ");
                    phoneNumber = Console.ReadLine();
                }
                if (!Int64.TryParse(phoneNumber, out x)) // Если пользователь ввел строку состоящую не из цифр
                {
                    Console.WriteLine($"{name}, Вы ввели не цифры! ");
                    Console.WriteLine("Введите номер телефона (Номер телефона должен состоять из цифр!): ");
                    phoneNumber = Console.ReadLine();
                }
            }
            Console.Clear();
            Console.WriteLine("Введите страну (это поле является обязательным!): "); // Просим ввести страну для записи в телефоной книжки
            string country = Console.ReadLine();
            while (String.IsNullOrEmpty(country)) // Если пользователь не введет страну (обязательное поле)
            {
                Console.WriteLine($"{name}, Вы ввели пустую строку! ");
                Console.WriteLine("Введите страну (это поле является обязательным!): ");
                country = Console.ReadLine();
            }
            Console.Clear();
            Console.WriteLine("Введите дату рождения (Это поле можно оставить пустым): ");
            string dataBirthday = Console.ReadLine();
            while (true)
            {
                if (String.IsNullOrEmpty(dataBirthday)) // Если пользователь оставил пустую строку, то поле дата рождения остается пустым
                {
                    break;
                }
                if (!DateTime.TryParse(dataBirthday, out DateTime data)) // Если пользователь ввел дату некорректно
                {
                    Console.WriteLine($"{name}, Вы пытались ввести дату, но она введена некорректно!");
                    Console.WriteLine("Введите дату рождения (Это поле можно оставить пустым): ");
                    dataBirthday = Console.ReadLine();
                } else 
                {
                    break;
                }
            }
            Console.Clear();
            Console.WriteLine("Введите организацию (это поле можно оставить пустым): "); // Просим ввести организацию для записи в телефоной книжки
            string organization = Console.ReadLine();
            Console.Clear();
            Console.WriteLine("Введите должность (это поле можно оставить пустым): "); // Просим ввести должность для записи в телефоной книжки
            string position = Console.ReadLine();
            Console.Clear();
            Console.WriteLine("Введите прочие заметки (это поле можно оставить пустым): "); // Просим ввести прочие заметки для записи в телефоной книжки
            string other = Console.ReadLine();
            Console.Clear();
            Person person = new Person(surname, nameNote, Convert.ToInt64(phoneNumber), country, middleName, dataBirthday, organization, position, other); // Создаем новый экземпляр класса Person
            ListNotebook.Add(person); // Добовляем новую запись в лист ListNotebook
        }

        static int FindNote () // Метод, который находит пользователя
        {
            List<int> indexOfNote = new List<int>(); // Лист где хранятся индексы найденных записей
            if (ListNotebook.Count > 1) // Если в телефоной книжке больше чем одна запись
            {
                Console.WriteLine("Введите фамилию: "); // Просим ввести фамилию
                string surname = Console.ReadLine();
                while (String.IsNullOrEmpty(surname)) // Если пользователь не введет фамилию
                {
                    Console.WriteLine($"{name}, Вы ввели пустую строку!");
                    Console.WriteLine("Без фамилии я не могу найти пользователя :(");
                    Console.WriteLine("Введите фамилию (это поле является обязательным!): ");
                    surname = Console.ReadLine();
                }
                Console.Clear();
                Console.WriteLine("Введите имя: "); // Просим ввести имя
                string nameFind = Console.ReadLine();
                while (String.IsNullOrEmpty(nameFind)) // Если пользователь не введет имя
                {
                    Console.WriteLine($"{name}, Вы ввели пустую строку! ");
                    Console.WriteLine("Без имени я не могу найти пользователя :(");
                    Console.WriteLine("Введите имя (это поле является обязательным!): ");
                    nameFind = Console.ReadLine();
                }
                Console.Clear();
                Console.WriteLine("Введите номер телефона (Номер телефона должен состоять из цифр!): "); // Просим ввести номер телефона для поиска в телефоной книжки
                string phoneNumber = Console.ReadLine();
                long x;
                while (String.IsNullOrEmpty(phoneNumber) || !Int64.TryParse(phoneNumber, out x)) // Если пользователь не введет номер телефона или номер введен некорректно
                {
                    if (String.IsNullOrEmpty(phoneNumber)) // Если пользователь ввел пустую строку
                    {
                        Console.WriteLine($"{name}, Вы ввели пустую строку! ");
                        Console.WriteLine("Без номера телефона я не могу найти пользователя :(");
                        Console.WriteLine("Введите номер телефона (это поле является обязательным!): ");
                        phoneNumber = Console.ReadLine();
                    }
                    if (!Int64.TryParse(phoneNumber, out x)) // Если пользователь ввел строку состоящую не из цифр
                    {
                        Console.WriteLine($"{name}, Вы ввели не цифры! ");
                        Console.WriteLine("Введите номер телефона (Номер телефона должен состоять из цифр!): ");
                        phoneNumber = Console.ReadLine();
                    }
                }
                Console.Clear(); // Отчистить консоль от предыдущих записей
                for (int i = 0; i < ListNotebook.Count; i++) // Ищем в ListNotebook подходящие записи на запрос от пользователя
                {
                    if (ListNotebook[i].surname == surname && ListNotebook[i].name == nameFind && ListNotebook[i].phoneNumber == Convert.ToInt64(phoneNumber))
                    {
                        indexOfNote.Add(i);
                    }
                }
            } else // Если в телефоной книжке есть только одна запись
            {
                indexOfNote.Add(0);
            }
            if (indexOfNote.Count > 0) // Если нашлись записи, то выводим их на экран
            {
                if (indexOfNote.Count > 1) // Если нашлось несколько записей
                {
                    Console.WriteLine($"{name}, вот что мне удалось найти: ");
                    PrintNotesWithNum(indexOfNote); // Выводи все найденные записи
                    Console.WriteLine($"Введите номер строки, чтобы выбрать [1 - {indexOfNote.Count}]: "); // Просим пользователя выбрать только одну запись из найденных 
                    string indexString = Console.ReadLine();
                    while (String.IsNullOrEmpty(indexString) || Convert.ToInt32(indexString) < 1 || Convert.ToInt32(indexString) > indexOfNote.Count) // Если пользователь введет некорректно индекс записи
                    {
                        Console.WriteLine($"{name}, Вы вели некорректно номер строки!");
                        Console.WriteLine($"Введите номер строки, чтобы выбрать [1 - {indexOfNote.Count}]: ");
                        indexString = Console.ReadLine();
                    }
                    int index = Convert.ToInt32(indexString);
                    return indexOfNote[index - 1]; // Возращаем индекс записи в ListNotebook
                } else
                {
                    return indexOfNote[0]; // Если нашлась только одна запись
                }
            }
            else // Если не нашли записи, то сообщаем пользователю, что такой записи не существует с такими данными
            {
                Console.WriteLine("С такими данными записи не существует!");
                return -1;
            }
        }

        static void EditNote(int index) // Метод для редоктирования записи
        {
            Console.WriteLine("Введите фамилию (Если Вы не хотите изменять это поле, то оставьте его пустым): "); // Просим ввести фамилию
            string surname = Console.ReadLine();
            if (!String.IsNullOrEmpty(surname))
            {
                ListNotebook[index].surname = surname;
            }
            Console.Clear();
            PrintNote(index); // перерисовываем запись в телефонной книжке после изменений 
            Console.WriteLine("Введите имя (Если Вы не хотите изменять это поле, то оставьте его пустым): "); // Просим ввести имя
            string nameEdit = Console.ReadLine();
            if (!String.IsNullOrEmpty(nameEdit))
            {
                ListNotebook[index].name = nameEdit;
            }
            Console.Clear();
            PrintNote(index); // перерисовываем запись в телефонной книжке после изменений 
            Console.WriteLine("Введите отчество (Если Вы не хотите изменять это поле, то оставьте его пустым): "); // Просим ввести отчество 
            string middleName = Console.ReadLine();
            if (!String.IsNullOrEmpty(middleName))
            {
                ListNotebook[index].middleName = middleName;
            }
            Console.Clear();
            PrintNote(index); // перерисовываем запись в телефонной книжке после изменений 
            Console.WriteLine("Введите номер телефона (Если Вы не хотите изменять это поле, то оставьте его пустым. Номер должен содержать только цифры!): "); // Просим ввести номер телефона
            string phoneNumber = Console.ReadLine();
            while (true)
            {
                if (String.IsNullOrEmpty(phoneNumber)) // Если пользователь решил не менять номер
                {
                    break;
                }
                if (!Int64.TryParse(phoneNumber, out long x)) // Если пытался изменить, но ввел некорректно номер
                {
                    Console.WriteLine($"{name}, Вы пытались ввести номер телефона, но он содержит не цифры!");
                    Console.WriteLine("Введите номер телефона (Если Вы не хотите изменять это поле, то оставьте его пустым. Номер должен содержать только цифры!): ");
                    phoneNumber = Console.ReadLine();
                } else // Если ввел номер телефон и он введен корректно
                {
                    ListNotebook[index].phoneNumber = Convert.ToInt64(phoneNumber);
                    break;
                }
            }
            Console.Clear();
            PrintNote(index); // перерисовываем запись в телефонной книжке после изменений 
            Console.WriteLine("Введите страну (Если Вы не хотите изменять это поле, то оставьте его пустым): "); // Просим ввести страну
            string country = Console.ReadLine();
            if (!String.IsNullOrEmpty(country))
            {
                ListNotebook[index].country = country;
            }
            Console.Clear();
            PrintNote(index); // перерисовываем запись в телефонной книжке после изменений 
            Console.WriteLine("Введите дату рождения (Если Вы не хотите изменять это поле, то оставьте его пустым): "); // Просим ввести дату рождения 
            string dataBirthday = Console.ReadLine();
            while (true)
            {
                if (String.IsNullOrEmpty(dataBirthday)) // Если пользователь оставил пустую строку, то поле дата рождения не изменится
                {
                    break;
                }
                if (!DateTime.TryParse(dataBirthday, out DateTime data)) // Если пользователь ввел дату некорректно
                {
                    Console.WriteLine($"{name}, Вы пытались ввести дату, но она введена некорректно!");
                    Console.WriteLine("Введите дату рождения (Если Вы не хотите изменять это поле, то оставьте его пустым): ");
                    dataBirthday = Console.ReadLine();
                }
                else // Если ввел дату корректно
                {
                    ListNotebook[index].dataBirthday = data;
                    break;
                }
            }
            Console.Clear();
            PrintNote(index); // перерисовываем запись в телефонной книжке после изменений 
            Console.WriteLine("Введите организацию (Если Вы не хотите изменять это поле, то оставьте его пустым): "); // Просим ввести организацию
            string organization = Console.ReadLine();
            if (!String.IsNullOrEmpty(organization))
            {
                ListNotebook[index].organization = organization;
            }
            Console.Clear();
            PrintNote(index); // перерисовываем запись в телефонной книжке после изменений 
            Console.WriteLine("Введите должность (Если Вы не хотите изменять это поле, то оставьте его пустым): "); // Просим ввести должность 
            string position = Console.ReadLine();
            if (!String.IsNullOrEmpty(position))
            {
                ListNotebook[index].position = position;
            }
            Console.Clear();
            PrintNote(index); // перерисовываем запись в телефонной книжке после изменений 
            Console.WriteLine("Введите прочие заметки (Если Вы не хотите изменять это поле, то оставьте его пустым): "); // Просим ввести заметки
            string other = Console.ReadLine();
            if (!String.IsNullOrEmpty(other))
            {
                ListNotebook[index].other = other;
            }
            Console.Clear();
            Console.WriteLine($"{name}, запись успешно изменена!");
            PrintNote(index); // перерисовываем запись в телефонной книжке после изменений 
        }
    }
    public class Person // Класс отвечающий за основные данные
    {
        public string surname; // Фамилия
        public string name; // Имя
        public string middleName; // Отчество
        public long phoneNumber; // Номер телефона
        public string country; // Страна
        public DateTime dataBirthday; // Дата рождения
        public string organization; // Организация
        public string position; // Должность
        public string other; // Прочие заметки
        public Person(string surname, string name, long phoneNumber, string country, string middleName, string dataBirthday,
            string organization, string position, string other) // Конструктор
        {
            this.surname = surname;
            this.name = name;
            this.phoneNumber = phoneNumber;
            this.country = country;
            if (!String.IsNullOrEmpty(middleName)) // Если пользователь не введет отчество, то к этому полю присвоем значение: "не указано"
            {
                this.middleName = middleName;
            } else
            {
                this.middleName = "не указано";
            }
            if (!String.IsNullOrEmpty(dataBirthday))
            {
                DateTime.TryParse(dataBirthday, out this.dataBirthday); // Если строка dataBirthday не пустая, то присваем значение 
            }
            if (!String.IsNullOrEmpty(organization)) // Если пользователь не введет организацию, то к этому полю присвоем значение: "не указано"
            {
                this.organization = organization;
            }
            else
            {
                this.organization = "не указано";
            }
            if (!String.IsNullOrEmpty(position)) // Если пользователь не введет должность, то к этому полю присвоем значение: "не указано"
            {
                this.position = position;
            }
            else
            {
                this.position = "не указано";
            }
            if (!String.IsNullOrEmpty(other))  // Если пользователь не введет прочие заметки, то к этому полю присвоем значение: "не указано"
            {
                this.other = other;
            }
            else
            {
                this.other = "не указано";
            }
        }
    }
}
